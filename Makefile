image = django_tutorial
container_name = django
port = 8000

commands:
	@echo "Usage: make [command]"
	@echo ""
	@echo ""
	@echo "Cleanup"
	@echo "----------------"
	@echo ""
	@echo "stop - stop container"
	@echo ""
	@echo "rm - remove container"
	@echo ""
	@echo "rmi - remove image"
	@echo ""
	@echo "stop_rm - stop container + remove container"
	@echo ""
	@echo "rm_rmi - remove container + remove image"
	@echo ""
	@echo "full_cleanup - stop container + remove container + remove image"
	@echo ""
	@echo ""
	@echo "Container Build"
	@echo "----------------"
	@echo ""
	@echo "build - build image"
	@echo ""
	@echo ""
	@echo "Container Run"
	@echo "----------------"
	@echo ""
	@echo "run_it - run interactive shell inside container"
	@echo ""
	@echo "run_d - run dettached"
	@echo ""
	@echo ""
	@echo "Compound"
	@echo "----------------"
	@echo ""
	@echo "build_run_it - build image + run interactive shell inside container"
	@echo ""
	@echo ""

# Cleanup
#---------------------------

stop:
	docker stop $(container_name)

rm: 
	docker rm $(container_name)

rmi: 
	docker rmi $(image)

stop_rm: stop rm

rm_rmi: rm rmi

full_cleanup: stop rm rmi


# Container Build
#---------------------------

build:
	docker build -t $(image) .


# Container Run
#---------------------------

run_it:
	docker run --name $(container_name) -it -p $(port):$(port) -v $(PWD):/app $(image) bash

run_d:
	docker run --name $(container_name) -d -p $(port):$(port) $(image)


# Compound
#---------------------------

build_run_it: build run_it